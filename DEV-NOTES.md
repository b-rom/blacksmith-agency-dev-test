# Dev Notes

**UPDATE**: Based on [README.md](./README.md) *("Test Instructions")*, I'm replacing [developer-refactor-notes.txt](./developer-refactor-notes.txt) with the [CHANGELOG.md](./CHANGELOG.md) file.

**UPDATE**: I created a Bitbucket repo for this project, so you can inspect my work a lot easier.

**UPDATE**: I made 4 changes prior to creating the git repo (see below).

1. Header Navigation showing numbers 01-04 - deleted extra code lines - navigation.php (19/67/73/79)
1. Hero Header Title line break issue - added <br /> tag - hero.php (22)
1. Hero Content text block is too narrow - increased x-large desktop width - _hero.scss (340)
1. Hero Main Picture background image blend is on wrong side - extended --right sass class and increased width - _hero.scss (197/198)

###### ©2020 Blacksmith Agency / License: Proprietary
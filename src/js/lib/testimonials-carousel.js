var $ = require( "jquery" );
require("slick-carousel");


(function () {
    
	let slider = $('#testimonials-carousel');

    Number.prototype.doubleDigits = function() {
        let value = this.valueOf();
        return value > 9 ? "" + value: "0" + value;
    }

    function getTotalSlideCount(slick_object) {
        let count = 0;
        slick_object.find('.slick-slide:not(.slick-cloned)').map(function(index, value) {
            count = index;
        });
        return Number(count + 1).doubleDigits();
    }

    function getCurrentSlideCount(slick_object) {
        return Number(slick_object.slick('slickCurrentSlide') + 1).doubleDigits();
    }
    
    function updateCurrentSlideCount(slick_object) {
        $('.testimonials-carousel-nav__pagination-count--current').html(getCurrentSlideCount(slick_object));
    }

    function updateTotalSlideCount(slick_object) {
        $('.testimonials-carousel-nav__pagination-count--total').html(getTotalSlideCount(slick_object));
    }

    function sliderEventHandler(event) {
        switch(event.type) {
            case 'init':
            case 'reInit':
            case 'afterChange':
                updateCurrentSlideCount(slider);
                break;
            default:
                console.log('unhandled slider event = %s', event.type);
                break;
        }
    }

    function init() {
        
        slider.slick({
            slidesToShow: 3,
            arrows: true,
            nextArrow: '.testimonials-carousel-nav__buttons--next',
            prevArrow: '.testimonials-carousel-nav__buttons--previous',
            // slidesToScroll: 1,
            // autoplay: true,
            // autoplaySpeed: 2000,
            responsive: [
            {
                breakpoint: 480,
                settings: {
                    arrows: true,
                    centerMode: true,
                    centerPadding: '20px',
                    slidesToShow: 1
                }
            }
            ]
        });

        slider.on('init reInit afterChange', sliderEventHandler);

        updateCurrentSlideCount(slider);
        updateTotalSlideCount(slider);
    }

    init();

})();

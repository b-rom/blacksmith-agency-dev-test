# 1.0.0 (2020-06-19)


### Bug Fixes

* added white bg and setup flex layout. ([661dad1](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/commits/661dad15d75a1e0ea75abe6b215d6fb0792993cf)), closes [#9](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/issue/9)
* adjusted format of hero pictures. ([b74bfdd](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/commits/b74bfdda4e757a6f232b3c330de24b48317dab07)), closes [#5](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/issue/5)
* decreased hero mobile paragraph width. ([5544cc6](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/commits/5544cc63cc7b617751f5de8a0e8e02189d590127)), closes [#14](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/issue/14)
* formatted images for company culture. ([e9b7ebe](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/commits/e9b7ebe0aad45a7dc10650680878727db8e3a223)), closes [#7](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/issue/7)
* open roles table formatting. ([62d26b4](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/commits/62d26b4bdc3ec5fa492f4fc05b24fd827fb6cdd0)), closes [#6](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/issue/6)
* renamed icon.svg to welding-icon.svg. ([9e3d60c](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/commits/9e3d60c31d0c896ac06e8247b88e6db6c17baa08)), closes [#10](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/issue/10)
* setup media query formatting for image burn. ([b67b216](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/commits/b67b216f1240b702a571656cbcf6d347f2f9f72c)), closes [#13](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/issue/13)
* setup mobile navigation numbers/formatting. ([06835f1](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/commits/06835f11e04506cd4c81079eafe9e7a4be972fc2)), closes [#12](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/issue/12)
* setup ordering in PHP and fixed Flex styling. ([0d2a503](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/commits/0d2a503b97ed3167e2120967805767d0466e2457)), closes [#8](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/issue/8)
* setup padding for mobile culture content. ([f87dd90](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/commits/f87dd9019b88a4dd322202883018c2cd7a1266a8)), closes [#15](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/issue/15)
* setup service buttons flex layout. ([fb36bda](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/commits/fb36bdab8ca84bbdbed9a3bb54969bc53a0ca126)), closes [#11](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/issue/11)
* setup testimonials slider carousel. ([fa7e453](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/commits/fa7e4539b23832ae7c19d9c798e9037ba06b0aa2)), closes [#16](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/issue/16)



# 1.0.0 (2020-06-17)


### Bug Fixes

* adjusted format of hero pictures. ([b74bfdd](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/commits/b74bfdda4e757a6f232b3c330de24b48317dab07)), closes [#5](https://bitbucket.org/b-rom/blacksmith-agency-dev-test/issue/5)



